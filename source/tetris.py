import pyxel as pxl
from random import choice
import time

# my classes
from parameters import *
from blocks import Block


class Tetris:
    def __init__(self):
        # music
        pxl.play(0, 0, 30, True)

        # timer
        self.init_time = time.time()
        self.timer = 0

        # the array of the cube contain in the block that are freeze
        self.map = []

        # the array of the different blocks
        self.block_name = ["line", "square", "z", "s", "pyramid", "l", "j"]

        # the one that you can move and the next one
        self.block = Block(SPAWN_COORDONATE[0], SPAWN_COORDONATE[1], choice(self.block_name), self.map)
        self.next_block = Block(COORDONATE_NEXT_BLOCK[0], COORDONATE_NEXT_BLOCK[1], choice(self.block_name), self.map)

        # time to move the block
        self.reset_time_down = 25
        self.time_down = self.reset_time_down
        self.reset_time_side = 2  # number of frame between pressing the side keys
        self.reset_time_down_fast = 1  # number of frame between pressing the down key (to go faster down)
        self.time_left, self.time_right = self.reset_time_side, self.reset_time_side
        self.time_faster_down = self.reset_time_down_fast

        # rotating
        self.rotating = False

        # scoring
        self.score = 0
        self.best_score = None
        self.best_timer = None
        with open(FILENAME, "r+") as file:
            for line in file:
                if self.best_timer is None:
                    self.best_timer = int(line)
                else:
                    self.best_score = int(line)

        # loose
        self.loose = False

    def new_block(self):
        # freeze the block and respawn another one
        if self.block.update_freeze():
            for cube in self.block.tab_cube:
                self.map.append(cube)
            self.block = Block(SPAWN_COORDONATE[0], SPAWN_COORDONATE[1], self.next_block.typ, self.map)
            self.next_block = Block(COORDONATE_NEXT_BLOCK[0], COORDONATE_NEXT_BLOCK[1], choice(self.block_name),
                                    self.map)
            self.del_line()

            # game over
            for cube in self.block.tab_cube:
                if cube.collision(self.map):
                    self.loose = True

                    # download the best score
                    if self.best_score < self.score:
                        self.best_timer = self.timer
                    self.best_score = max(self.score, self.best_score)
                    with open(FILENAME, "w+") as file:
                        file.write(str(self.best_timer) + "\n")
                        file.write(str(self.best_score))

                    break

    def del_line(self):
        # create a representation of the map in a matrix
        mat = [[None for k in range(APP_SIZE[0] // CUBE_SIZE)] for k1 in range(APP_SIZE[1] // CUBE_SIZE)]
        for cube in self.map:
            mat[cube.y // CUBE_SIZE][cube.x // CUBE_SIZE] = cube

        # delete the lines complete and get down the line above
        nb_line_delete = 0
        for i in range(1, len(mat)+1):
            if None not in mat[-i]:  # line is complete
                nb_line_delete += 1

                # delete the line complete
                for cube in mat[-i]:
                    self.map.remove(cube)

                # get down all the cube above that line
                for j in range(1, len(mat) - i):
                    for cube in mat[j]:
                        if cube is not None:
                            cube.down()

        if nb_line_delete != 0:
            if nb_line_delete == 1:
                self.score += 100
            elif nb_line_delete == 2:
                self.score += 300
            elif nb_line_delete == 3:
                self.score += 500
            else:
                self.score += 800

    def update(self):
        if not self.loose:
            # timer
            self.timer = int(time.time() - self.init_time)

            # rotate
            if pxl.btn(pxl.KEY_Z):
                if not self.rotating:
                    self.block.rotate()
                    self.rotating = True
            elif self.rotating:
                self.rotating = False

            # move left
            if pxl.btn(pxl.KEY_LEFT):
                if self.time_left <= 0:
                    self.block.left()
                    self.time_left = self.reset_time_side
                else:
                    self.time_left -= 1

            # move right
            if pxl.btn(pxl.KEY_RIGHT):
                if self.time_right <= 0:
                    self.block.right()
                    self.time_right = self.reset_time_side
                else:
                    self.time_right -= 1

            # move faster down
            if pxl.btn(pxl.KEY_DOWN):
                if self.time_faster_down <= 0:
                    self.new_block()
                    self.block.down()
                    self.time_faster_down = self.reset_time_down_fast
                else:
                    self.time_faster_down -= 1
            else:
                # put down the block automatically
                if self.time_down <= 0:
                    self.new_block()
                    self.block.down()
                    self.time_down = self.reset_time_down
                else:
                    self.time_down -= 1
        else:
            if pxl.btn(pxl.KEY_SPACE):
                self.loose = False
                self.map = []

                # the one that you can move and the next one
                self.block = Block(SPAWN_COORDONATE[0], SPAWN_COORDONATE[1], choice(self.block_name), self.map)
                self.next_block = Block(COORDONATE_NEXT_BLOCK[0], COORDONATE_NEXT_BLOCK[1], choice(self.block_name),
                                        self.map)

                # timer
                self.init_time = time.time()
                self.timer = 0

                self.score = 0

    def draw(self):
        if not self.loose:
            # display the one that you can move
            self.block.display()

            # display the freeze blocks
            for block in self.map:
                block.display()
        else:
            txt = "GAME OVER"
            pxl.text(APP_SIZE[0]//2 - len(txt) * 2, APP_SIZE[1] // 2 - 3, txt, 7)
            txt = "press 'SPACE' to retry"
            pxl.text(APP_SIZE[0] // 2 - len(txt) * 2, APP_SIZE[1] // 2 + 10, txt, 7)
