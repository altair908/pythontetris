# Size
CUBE_SIZE = 16
APP_SIZE = (10*CUBE_SIZE, 20*CUBE_SIZE)
SCREEN_SIZE = (APP_SIZE[0] + 70, APP_SIZE[1])

SPAWN_COORDONATE = (APP_SIZE[0] // 2 - CUBE_SIZE, 0)
COORDONATE_NEXT_BLOCK = (APP_SIZE[0] + 10, 20)

# download the scores
FILENAME = "score.txt"
