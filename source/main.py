import pyxel as pxl

# my classes
from parameters import *
from tetris import Tetris


class App:
    def __init__(self):
        pxl.init(SCREEN_SIZE[0], SCREEN_SIZE[1], display_scale=2)
        pxl.load("ressources.pyxres")

        self.tetris = Tetris()

        pxl.run(self.update, self.draw)

    def update(self):
        # tetris update
        self.tetris.update()

    def draw(self):
        pxl.cls(0)

        # tetris display
        self.tetris.draw()

        # display the background of the side
        pxl.rect(APP_SIZE[0], 0, SCREEN_SIZE[0] - APP_SIZE[0], pxl.height, 5)

        x = APP_SIZE[0] + (SCREEN_SIZE[0] - APP_SIZE[0])//2

        # display the timer
        txt = "TIMER: " + str(self.tetris.timer) + "s"
        pxl.text(x - 2 * len(txt) + 2, SCREEN_SIZE[1] // 2, txt, 7)

        # display the score
        pxl.text(x - 2 * len("SCORE") + 2, SCREEN_SIZE[1] // 3 * 2, "SCORE:", 7)
        pxl.text(x - 2 * len(str(self.tetris.score)) + 2, SCREEN_SIZE[1] // 3 * 2 + 10, str(self.tetris.score), 7)

        # display your best score with the best timer
        pxl.text(x - 2 * len("BEST SCORE :") + 2, SCREEN_SIZE[1] - 40, "BEST SCORE :", 7)
        txt = str(self.tetris.best_timer) + "s:   " + str(self.tetris.best_score)
        pxl.text(x - 2 * len(txt), SCREEN_SIZE[1] - 30, txt, 7)

        # display the next block
        if not self.tetris.loose:
            self.tetris.next_block.display()


if __name__ == "__main__":
    App()
