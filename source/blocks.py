import pyxel as pxl

# my classes
from parameters import *


class Cube:
    def __init__(self, x, y, col=7):
        self.x, self.y = x, y
        self.color = col

    def collision(self, map1):
        for cube in map1:
            if cube.x == self.x and cube.y == self.y:
                return True
        return False

    def collision_down(self, map1):
        for cube in map1:
            if cube.x == self.x and cube.y == self.y + CUBE_SIZE:
                return True
        return False

    def collision_left(self, map1):
        for cube in map1:
            if cube.x == self.x - CUBE_SIZE and cube.y == self.y:
                return True
        return False

    def collision_right(self, map1):
        for cube in map1:
            if cube.x == self.x + CUBE_SIZE and cube.y == self.y:
                return True
        return False

    def down(self):
        self.y += CUBE_SIZE

    def left(self):
        self.x -= CUBE_SIZE

    def right(self):
        self.x += CUBE_SIZE

    def display(self):
        pxl.blt(self.x, self.y, 0, self.color % 4 * CUBE_SIZE, self.color // 4 * CUBE_SIZE, CUBE_SIZE, CUBE_SIZE)


def get_cubes_by_typ(x, y, typ, img):
    tab_cube = []

    if typ == "line":
        tab_cube = [[Cube(x, y, 0), Cube(x + CUBE_SIZE, y, 0), Cube(x + CUBE_SIZE * 2, y, 0),
                    Cube(x + CUBE_SIZE * 3, y, 0)],
                    [Cube(x, y, 0), Cube(x, y + CUBE_SIZE, 0), Cube(x, y + CUBE_SIZE * 2, 0),
                     Cube(x, y + CUBE_SIZE * 3, 0)]]
        return tab_cube[img % 2]
    elif typ == "square":
        tab_cube = [Cube(x, y, 1), Cube(x + CUBE_SIZE, y, 1), Cube(x, y + CUBE_SIZE, 1),
                    Cube(x + CUBE_SIZE, y + CUBE_SIZE, 1)]
        return tab_cube
    elif typ == "z":
        tab_cube = [[Cube(x, y, 2), Cube(x + CUBE_SIZE, y, 2), Cube(x + CUBE_SIZE, y + CUBE_SIZE, 2),
                    Cube(x + CUBE_SIZE * 2, y + CUBE_SIZE, 2)],
                    [Cube(x + CUBE_SIZE, y, 2), Cube(x + CUBE_SIZE, y + CUBE_SIZE, 2), Cube(x, y + CUBE_SIZE, 2),
                    Cube(x, y + CUBE_SIZE * 2, 2)]]
        return tab_cube[img % 2]
    elif typ == "s":
        tab_cube = [[Cube(x, y + CUBE_SIZE, 3), Cube(x + CUBE_SIZE, y + CUBE_SIZE, 3), Cube(x + CUBE_SIZE, y, 3),
                    Cube(x + CUBE_SIZE * 2, y, 3)],
                    [Cube(x, y, 3), Cube(x, y + CUBE_SIZE, 3), Cube(x + CUBE_SIZE, y + CUBE_SIZE, 3),
                    Cube(x + CUBE_SIZE, y + CUBE_SIZE * 2, 3)]]
        return tab_cube[img % 2]
    elif typ == "pyramid":
        tab_cube = [[Cube(x, y + CUBE_SIZE, 4), Cube(x + CUBE_SIZE, y + CUBE_SIZE, 4), Cube(x + CUBE_SIZE, y, 4),
                    Cube(x + CUBE_SIZE * 2, y + CUBE_SIZE, 4)],
                    [Cube(x, y, 4), Cube(x, y + CUBE_SIZE, 4), Cube(x + CUBE_SIZE, y + CUBE_SIZE, 4),
                    Cube(x, y + CUBE_SIZE * 2, 4)],
                    [Cube(x, y, 4), Cube(x + CUBE_SIZE, y, 4), Cube(x + CUBE_SIZE, y + CUBE_SIZE, 4),
                    Cube(x + CUBE_SIZE * 2, y, 4)],
                    [Cube(x, y + CUBE_SIZE, 4), Cube(x + CUBE_SIZE, y, 4), Cube(x + CUBE_SIZE, y + CUBE_SIZE, 4),
                    Cube(x + CUBE_SIZE, y + CUBE_SIZE * 2, 4)]]
    elif typ == "l":
        tab_cube = [[Cube(x, y, 5), Cube(x, y + CUBE_SIZE, 5), Cube(x, y + CUBE_SIZE * 2, 5),
                    Cube(x + CUBE_SIZE, y + CUBE_SIZE * 2, 5)],
                    [Cube(x, y, 5), Cube(x, y + CUBE_SIZE, 5), Cube(x + CUBE_SIZE, y, 5),
                    Cube(x + CUBE_SIZE * 2, y, 5)],
                    [Cube(x, y, 5), Cube(x + CUBE_SIZE, y, 5), Cube(x + CUBE_SIZE, y + CUBE_SIZE, 5),
                     Cube(x + CUBE_SIZE, y + CUBE_SIZE * 2, 5)],
                    [Cube(x, y + CUBE_SIZE, 5), Cube(x + CUBE_SIZE, y + CUBE_SIZE, 5), Cube(x + CUBE_SIZE * 2, y + CUBE_SIZE, 5),
                    Cube(x + CUBE_SIZE * 2, y, 5)]]
    elif typ == "j":
        tab_cube = [[Cube(x, y + CUBE_SIZE * 2, 6), Cube(x + CUBE_SIZE, y + CUBE_SIZE * 2, 6),
                    Cube(x + CUBE_SIZE, y + CUBE_SIZE, 6), Cube(x + CUBE_SIZE, y, 6)],
                    [Cube(x, y, 6), Cube(x, y + CUBE_SIZE, 6),
                     Cube(x + CUBE_SIZE, y + CUBE_SIZE, 6), Cube(x + CUBE_SIZE * 2, y + CUBE_SIZE, 6)],
                    [Cube(x, y, 6), Cube(x + CUBE_SIZE, y, 6),
                     Cube(x, y + CUBE_SIZE, 6), Cube(x, y + CUBE_SIZE * 2, 6)],
                    [Cube(x, y, 6), Cube(x + CUBE_SIZE, y, 6),
                     Cube(x + CUBE_SIZE * 2, y, 6), Cube(x + CUBE_SIZE * 2, y + CUBE_SIZE, 6)]]

    return tab_cube[img]


class Block:
    def __init__(self, x, y, typ, map1):
        self.x, self.y = x, y
        self.img, self.typ = 0, typ
        self.tab_cube = get_cubes_by_typ(self.x, self.y, self.typ, self.img)
        self.map = map1

        self.freeze = False

    def rotate(self):
        img = (self.img + 1) % 4
        tab_cube = get_cubes_by_typ(self.x, self.y, self.typ, img)
        for cube in tab_cube:
            if (cube.collision(self.map) or not 0 <= cube.x <= APP_SIZE[0] - CUBE_SIZE or
                    not 0 <= cube.y <= APP_SIZE[1] - CUBE_SIZE):
                return
        self.img = img
        self.tab_cube = tab_cube

    def update_freeze(self):
        if not self.freeze:
            for cube in self.tab_cube:
                if cube.y + CUBE_SIZE >= APP_SIZE[1] or cube.collision_down(self.map):
                    self.freeze = True
                    return True
        return False

    def down(self):
        if not self.freeze:
            self.y += CUBE_SIZE
            for cube in self.tab_cube:
                cube.down()

    def left(self):
        if not self.freeze and CUBE_SIZE <= self.x:
            for cube in self.tab_cube:
                if cube.collision_left(self.map):
                    return
            self.x -= CUBE_SIZE
            for cube in self.tab_cube:
                cube.left()

    def right(self):
        if not self.freeze and APP_SIZE[0] >= self.x+CUBE_SIZE:
            # verify that you can move the block in this direction
            for cube in self.tab_cube:
                if cube.x + CUBE_SIZE >= APP_SIZE[0] or cube.collision_right(self.map):
                    return
            self.x += CUBE_SIZE
            for cube in self.tab_cube:
                cube.right()

    def display(self):
        for cube in self.tab_cube:
            cube.display()
