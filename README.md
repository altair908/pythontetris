# PythonTetris

## Description
This project is a Tetris made with python. Like you already know, you can stack piece together, try to complete lines to destroy them. The goal is to have the best score possible.

## Instalation
To run this project, you'll need to download Pyxel (module for the display of the game)  
  
* Install Pyxel on **Linux**:  
    * First download the files
    * Then create a .venv in the source directory by doing: **sudo apt install python3-venv; python3 -m venv .venv**
    * activate the .venv: **source .venv/bin/activate**
    * install pyxel in the .venv: **pip install pyxel**
    * run the project: **python3 main.py**
    * dont forget to deactivate the .venv: **deactivate**

## Usage
You can use the arrows to move the block, and you can use the "Z" key to rotate them

## Advancement
This project is finish, but I'll maybe do an unbeatable AI
